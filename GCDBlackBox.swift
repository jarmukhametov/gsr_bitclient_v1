//
//  GCDBlackBox.swift
//  GSR_Bit_Client
//
//  Created by User on 20.09.2018.
//  Copyright © 2018 GSR. All rights reserved.
//

import Foundation

func performUIUpdatesOnMain(_ updates: @escaping () -> Void) {
    DispatchQueue.main.async {
        updates()
    }
}
