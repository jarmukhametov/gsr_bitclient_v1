//
//  DecisionCell.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

internal final class UserDecisionCell: UITableViewCell, Cell {
    // Outlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var databaseLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var visaLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        dateLabel.text = nil
        databaseLabel.text = nil
        linkLabel.text = nil
        visaLabel.text = nil
    }
    
}

