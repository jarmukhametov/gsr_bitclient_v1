//
//  FieldCell.swift
//
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

internal final class FieldCell: UITableViewCell, Cell {
    // Outlets
    @IBOutlet weak var fieldLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        fieldLabel.text = nil
        
    }
    
}

