//
//  DocumentCell.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

internal final class DocumentCell: UITableViewCell, Cell {
    // Outlets
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var databaseLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        linkLabel.text = nil
        userLabel.text = nil
        databaseLabel.text = nil

    }
    
    
    
}
