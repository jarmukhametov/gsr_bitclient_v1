//
//  ForecastCell.swift
//  GsrBitClient
//
//  Created by Admin on 27.06.2019.
//  Copyright © 2019 GSR. All rights reserved.
//

import UIKit

class ForecastCell: UITableViewCell, Cell {

    @IBOutlet weak var forecastLabel: UILabel!
    @IBOutlet weak var applicationAmountLabel: UILabel!
    @IBOutlet weak var forecastAmountLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var finCenterLabel: UILabel!
    
    override func prepareForReuse() {
        forecastAmountLabel.text = nil
        applicationAmountLabel.text = nil
        forecastLabel.text = nil
        balanceLabel.text = nil
        finCenterLabel.text = nil
    }
}
