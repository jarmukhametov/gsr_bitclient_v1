//
//  FileCell.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

protocol FileCellDelegate {
    func actionPressed(_ fileCell: FileCell)
}

class FileCell: UITableViewCell, Cell {
    
    var delegate: FileCellDelegate?
    
    // Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var bntFile: UIButton!
    
    
    @IBAction func btnFilePressed(_ sender: Any) {
        delegate?.actionPressed(self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        descriptionLabel.text = nil
        titleLabel.text = nil
        bntFile.setImage(UIImage(named: "file"), for: .normal)
    }
    
}
