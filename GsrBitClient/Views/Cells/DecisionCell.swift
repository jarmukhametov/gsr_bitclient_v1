//
//  DecisionCell.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

internal final class DecisionCell: UITableViewCell, Cell {
    // Outlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var decisionLabel: UILabel!

    
    override func prepareForReuse() {
        super.prepareForReuse()
        dateLabel.text = nil
        userLabel.text = nil
        decisionLabel.text = nil

    }
    
}

