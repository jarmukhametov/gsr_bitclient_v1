//
//

import Foundation

struct Constants {
    
    static let resultCode = "ResultCode"
    static let costDocumentType = "бит_ЗаявкаНаРасходованиеСредств"
    
    static let documentTypes: [String: String] = [
        "бит_ЗаявкаНаРасходованиеСредств": "Заявка на расходование денежных средств",
        "бит_РеестрПлатежей": "Реестр платежей",
        "бит_ФормаВводаБюджета": "Форма ввода бюджета",
        "ГСР_Поручения": "Поручения",
        "ГСР_КорректировкаБюджета": "Корректировка бюджета",
        "бит_ЗаявкаНаРасходованиеСредствОбщая": "Прогноз платежа"
    ]
    
    // -------------------------------------------------------------------------
    // MARK: - Server params
    struct gsrServer {
        static let ApiScheme = "http"
        //static let ApiHost = "85.143.139.73"
        static let ApiHost = "217.79.4.251"
        //static let ApiPath = "/GSR_Gateway_Prod/hs/MobileAppClient/v1"
        static let ApiPath = "/GSR-Gateway/hs/MobileAppClient/v1"
        //static let ApiPort = 888
        static let ApiPort = 80
        static let ApiUser = "exchange"
        static let ApiPassword = "123456"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - Login
    
    // Params
    struct loginParams {
        static let method = "/login"
        static let user = "user"
        static let password = "passwordHash"
    }
    
    // Response
    struct loginResponse {
        static let userId = "UserID"
        static let token = "Token"
        static let isTreasusrer = "IsTreasurer"
        static let successCode = "0100"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - GetDocuments
    
    // Params
    struct getDocumentsParams {
        static let method = "/GetObjectList"
        static let token = "token"
    }
    
    // Response
    struct getDocumentsResponse {
        static let successCode = "0200"
        static let documents = "Documents"
    }
    
    // Document structure
    struct document {
        static let id = "id"
        static let user = "user"
        static let base = "base"
        static let number = "number"
        static let date = "date"
        static let view = "view"
        static let data = "data"
        static let userId = "userId"
        static let baseId = "baseId"
        static let type = "type"
        static let fields = "fields"
    }
    
    // Document field structure
    struct documentFields {
        static let description = "description"
        static let value = "value"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - GetFiles
    
    // Params
    struct getFilesParams {
        static let method = "/GetFileList"
        static let documentId = "DocumentId"
    }
    
    // Response
    struct getFilesResponse {
        static let successCode = "0400"
        static let files = "FileList"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - GetFile
    
    // Params
    struct getFileParams {
        static let method = "/GetFileById"
        static let fileId = "Id"
        static let baseId = "BaseId"
        static let documentId = "DocumentId"
    }
    
    // Response
    struct getFileResponse {
        static let successCode = "0500"
    }
    
    // File structure
    struct file {
        static let id = "id"
        static let documentId = "documentID"
        static let baseId = "baseId"
        static let fileName = "fileName"
        static let description = "description"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - GetDecisions
    
    // Params
    struct getDecisionsParams {
        static let method = "/GetDecisions"
        static let documentId = "DocumentId"
        static let type = "type"
        static let baseId = "BaseId"
    }
    
    // Response
    struct getDecisionResponse {
        static let successCode = "0600"
        static let decisions = "decisions"
    }
    
    // Decision structure
    struct decision {
        static let date = "date"
        static let visa = "visa"
        static let user = "user"
        static let comment = "comment"
        static let decision = "decision"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - SetDecision
    
    // Params
    struct setDecisionParams {
        static let method = "/SetDecision"
        static let token = "token"
        static let documentId = "DocumentId"
        static let decision = "Decision"
        static let comment = "Comment"
    }
    
    // MARK: - CreateUser
    
    // Params
    struct createUserParams {
        static let method = "/CreateUser"
        static let login = "Login"
        static let passwordHash = "PasswordHash"
        static let successCode = "0700"
        static let doubleErrorCode = "0701"
        static let errorCode = "0702"
    }
    
    // -------------------------------------------------------------------------
    // NARK: - GetIOSAppCurrentVersion
    
    // Params
    struct checkNewVersionParams {
        static let method = "/GetIOSAppCurrentVersion"
    }
    
    // Response
    struct checkNewVersionResponse {
        static let version = "version"
        static let path = "path"
        static let successCode = "1000"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - GetDecisionsByUser

    // Params
    struct  getDecisionsByUserParams {
        static let method = "/GetDecisionsByUser"
        static let token = "Token"
        static let showDays = "ShowDays"
    }
    
    // Response
    struct getDecisionsByUserResponse {
        static let successCode = "0800"
        static let decisions = "decisions"
    }
    
    // User decision structure
    struct userDecision {
        static let id = "id"
        static let base = "base"
        static let baseId = "baseId"
        static let number = "number"
        static let view = "view"
        static let date = "date"
        static let type = "type"
        static let visa = "visa"
        static let comment = "comment"
        static let decision = "decision"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - GetObject
    
    // Params
    struct getObjectParams {
        static let method = "/GetObject"
        static let type = "type"
        static let documentId = "documentId"
        static let baseId = "baseId"
    }
    
    // Response
    struct getObjectResponse {
        static let successCode = "0900"
        static let document = "document"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - SetCostDate
    
    // Params
    struct setCostDateParams {
        static let method = "/SetCostDate"
        static let id = "id"
        static let date = "date"
        static let baseId = "baseId"
        
    }
    
    // Response
    struct setCostDateResponse{
        static let successCode = "1100"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - GetPaymentsForecast
    
    // Params
    struct getPaymentForecastParams {
        static let method = "/GetPaymentsForecast"
        static let documentId = "documentId"
        static let baseId = "baseId"
    }
    
    // Response
    struct getPaymentForecastResponse {
        static let successCode = "1200"
        static let forecasts = "PaymentsForecast"
    }
    
    // Forecast structure
    struct forecast {
        static let paymentForecast = "PaymentForecast"
        static let paymentForecastId = "PaymentForecastId"
        static let forecastAmount = "ForecastAmount"
        static let applicationAmount = "ApplicationAmount"
        static let balance = "Balance"
        static let finCenter = "FinCenter"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - DeletePaymentForecast
    
    // Params
    struct deletePaymentForecastParams {
        static let method = "/DeletePaymentForecast"
        static let id = "id"
        static let forecastId = "ForecastId"
        static let baseId = "baseId"
    }
    
    // Response
    struct deletePaymentForecastResponse {
        static let successCode = "1300"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - GetPaymentsForecastForSelection
    
    // Params
    struct getPaymentsForecastForSelectionParams {
        static let method = "/GetPaymentsForecastForSelection"
        static let documentId = "documentId"
        static let baseId = "baseId"
    }
    
    // Response
    struct getPaymentsForecastForSelectionResponse {
        static let successCode = "1400"
        static let forecasts = "PaymentsForecast"
    }
    
    // -------------------------------------------------------------------------
    // MARK: - SetPaymentForecast
    
    // Params
    struct setPaymentForecastParams {
        static let method = "/SetPaymentForecast"
        static let id = "id"
        static let baseId = "baseId"
        static let forecastId = "ForecastId"
        static let forecastAmount = "ForecastAmount"
        static let applicationAmount = "ApplicationAmount"
        static let balance = "Balance"
    }
    
    // Response
    struct stPaymentForecastResponse {
        static let successCode = "1500"
    }
    
}
