//
//  DecisionViewController.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

private let reuseIdentifier = "DecisionCell"

class DecisionViewController: UITableViewController {
    	
    // MARK: Properties
    var appDelegate: AppDelegate!
    var document: Document!
    var decisions: [Decision] = [Decision]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        getDecisions()
        
    }
    
    private func getDecisions(){
        
        
        /* 1. Set the parameters */
        
        let methodParameters = [
            Constants.getDecisionsParams.documentId: document.id,
            Constants.getDecisionsParams.baseId: document.baseId,
            Constants.getDecisionsParams.type: document.type]
        
        /* 2/3. Build the URL, Configure the request */
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.getDecisionsParams.method))
        print("Url: \(request)")
        
        /* 4. Make the request */
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            
            // if an error occurs, print it and re-enable the UI
            func displayError(_ error: String, debugLabelText: String? = nil) {
                print(error)
                performUIUpdatesOnMain {
                    let alertController = UIAlertController(title: "Bit Client", message:
                        error, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            }
            
            func updateTable() {
                performUIUpdatesOnMain {
                    self.tableView.reloadData()
                }
            }
            
            
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                displayError("Сервер не найден")
                return
            }
            
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                displayError("Ошибка на сервере")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                displayError("Сервер не вернул данные")
                return
            }
            
            /* 5. Parse the data */
            
            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]
                //print("Result data: '\(String(describing: parsedResult))'")
            } catch {
                print("Could not parse the data as JSON: '\(data)'")
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                print("Cannot find key '\(Constants.resultCode)' in \(String(describing: parsedResult))")
                return
            }
            
            if (resultCode != Constants.getDecisionResponse.successCode){
                displayError("Не удалось получить историю согласования!")
                return
            }
            
            guard let decisions = parsedResult[Constants.getDecisionResponse.decisions] as? [[String:AnyObject]] else {
                print("Cannot find key '\(Constants.getDecisionResponse.decisions)' in \(parsedResult)")
                return
            }
            
            /* 6. Use the data! */
            self.decisions = Decision.decisionsFromResults(decisions)
            
            updateTable()
        }
        
        /* 6. Start the request */
        task.resume()
    }
    
    
    
    
    // -------------------------------------------------------------------------
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfDecisions
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentDecision = decision(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: "DecisionCell", for: indexPath) as! DecisionCell
        
        // Configure cell
        cell.dateLabel.text = currentDecision.date
        cell.userLabel.text = currentDecision.user
        cell.decisionLabel.text = currentDecision.visa + ": " + currentDecision.decision + (currentDecision.comment.isEmpty ? "" : "/" + currentDecision.comment)
        
        return cell
    }
    
    // Helper
    
    var numberOfDecisions: Int { return decisions.count }
    
    func decision(at indexPath: IndexPath) -> Decision {
        return decisions[indexPath.row]
    }
    
}
