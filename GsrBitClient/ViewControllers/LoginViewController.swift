//
//  LoginViewController.swift
//  GSR_Bit_Client
//
//  Created by User on 19.09.2018.
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: Properties
    var appDelegate: AppDelegate!

    // MARK: - Outlets
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var debugTextLabel: UILabel!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var currentVersionLabel: UILabel!
    
    // MARK: - Life Cycle

    override func viewWillAppear(_ animated: Bool) {
        debugTextLabel.text = ""
        setCurrentVersionText()
        checkNewVersion()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        restoreValues()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Login

    func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
    
    @IBAction func loginPressed(_ sender: AnyObject) {
        if loginTextField.text!.isEmpty
            || passwordTextField.text!.isEmpty {
            debugTextLabel.text = "Логин или пароль пустой..."
        }
        else{
            login()
            saveValues()
        }
    }
    
    private func login(){
        
        /* 1. Set the parameters */
        let md5Data = MD5(string:passwordTextField.text!)
        let md5Base64 = md5Data.base64EncodedString()
        let md5Hex =  md5Data.map { String(format: "%02hhx", $0) }.joined()

        let methodParameters = [
            Constants.loginParams.user: loginTextField.text!,
            Constants.loginParams.password: md5Hex]
        
        /* 2/3. Build the URL, Configure the request */
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.loginParams.method))
        print("Url: \(request)")
        
        /* 4. Make the request */
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            
            // if an error occurs, print it and re-enable the UI
            func displayError(_ error: String, debugLabelText: String? = nil) {
                print(error)
                performUIUpdatesOnMain {
                    self.debugTextLabel.text = error
                }
            }
            
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                displayError("Сервер не найден")
                return
            }
            
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                displayError("Ошибка на сервере")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                displayError("Сервер не вернул данные")
                return
            }
            
            /* 5. Parse the data */
            
            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]
                print("Result data: '\(String(describing: parsedResult))'")
            } catch {
                print("Could not parse the data as JSON: '\(data)'")
                return
            }
            
            
            guard let userID = parsedResult![Constants.loginResponse.userId] as? String else {
                print("Cannot find key '\(Constants.loginResponse.userId)' in \(String(describing: parsedResult))")
                return
            }
            
            guard let token = parsedResult![Constants.loginResponse.token] as? String else {
                print("Cannot find key '\(Constants.loginResponse.token)' in \(String(describing: parsedResult))")
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                print("Cannot find key '\(Constants.resultCode)' in \(String(describing: parsedResult))")
                return
            }
            
            guard let isTresurer = parsedResult![Constants.loginResponse.isTreasusrer] as?  Bool else {
                print("Cannot find key '\(Constants.loginResponse.isTreasusrer)' in \(String(describing: parsedResult))")
                return
            }
            
            if (resultCode == Constants.loginResponse.successCode){
                //  Open documents list
                self.appDelegate.token = token;
                self.appDelegate.userID = userID;
                self.appDelegate.isTreasurer = isTresurer
                performUIUpdatesOnMain{
                    //self.performSegue(withIdentifier: "showDocumentList", sender: self)                    
                    self.performSegue(withIdentifier: "ShowDocumentTypes", sender: self)
                }
            }
            else{
                displayError("Неверный логин или пароль!")
            }
            
        }
        
        /* 6. Start the request */
        task.resume()
    }
    
    // MARK: - Persist func
    
    func restoreValues(){
        loginTextField.text = UserDefaults.standard.string(forKey: "user")
        passwordTextField.text = UserDefaults.standard.string(forKey: "password")
    }
    
    func saveValues(){
        UserDefaults.standard.set(loginTextField.text, forKey: "user")
        UserDefaults.standard.set(passwordTextField.text, forKey: "password")
    }
    
    // MARK: - Check new version
    
    private func checkNewVersion(){
        
        /* 1. Set the parameters */
        
        /* 2/3. Build the URL, Configure the request */
        let request = URLRequest(url: appDelegate.URLFromParameters(nil, withPathExtension: Constants.checkNewVersionParams.method))
        print("Url: \(request)")
        
        /* 4. Make the request */
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            
            // if an error occurs, print it and re-enable the UI
            func displayError(_ error: String, debugLabelText: String? = nil) {
                print(error)
                performUIUpdatesOnMain {
                    self.debugTextLabel.text = error
                }
            }
            
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                displayError("Сервер не найден")
                return
            }
            
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                displayError("Ошибка на сервере")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                displayError("Сервер не вернул данные")
                return
            }
            
            /* 5. Parse the data */
            
            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]
                print("Result data: '\(String(describing: parsedResult))'")
            } catch {
                print("Could not parse the data as JSON: '\(data)'")
                return
            }
            
            
            guard let newVersion = parsedResult![Constants.checkNewVersionResponse.version] as? Int else {
                print("Cannot find key '\(Constants.checkNewVersionResponse.version)' in \(String(describing: parsedResult))")
                return
            }
            
            guard let path = parsedResult![Constants.checkNewVersionResponse.path] as? String else {
                print("Cannot find key '\(Constants.checkNewVersionResponse.path)' in \(String(describing: parsedResult))")
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                print("Cannot find key '\(Constants.resultCode)' in \(String(describing: parsedResult))")
                return
            }
            
            if (resultCode == Constants.checkNewVersionResponse.successCode){
                //  Show alert
                if let currentVersion = Int(Bundle.main.infoDictionary!["CFBundleVersion"] as! String) {
                    if (newVersion > currentVersion){
                        self.showNewVersionDialog(path: path);
                    }
                }
            }
            
        }
        
        /* 6. Start the request */
        task.resume()
    }
    
    func showNewVersionDialog(path: String){
        let alert = UIAlertController(title: "Обновление", message: "Доступна новая версия приложения", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Обновить", style: .default, handler: { action in
            if let url = URL(string: path){
                UIApplication.shared.openURL(url)
            }
        }))
        alert.addAction(UIAlertAction(title: "Использовать текущую", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Other function
    func setCurrentVersionText(){
        if let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String {
            currentVersionLabel.text = "v.  \(currentVersion)"
        }
        else {
            currentVersionLabel.text = ""
        }
    }

}
