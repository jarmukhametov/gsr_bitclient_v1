//
//  SelectForecastViewController.swift
//  GsrBitClient
//
//  Created by Admin on 27.06.2019.
//  Copyright © 2019 GSR. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ForecastCell"

class SelectForecastViewController: UITableViewController {

    // ------------------------------
    // MARK: - Properties
    var appDelegate: AppDelegate!
    var forecasts: [Forecast] = [Forecast]()
    var document: Document!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        getForecasts()
    }
    
    // -------------------------------
    // MARK: - Network requests
    
    func getForecasts(){
        
        forecasts.removeAll()
        
        /* 1. Set the parameters */
        let methodParameters = [
            Constants.getPaymentsForecastForSelectionParams.documentId: document.id,
            Constants.getPaymentsForecastForSelectionParams.baseId: document.baseId] as [String : AnyObject]
        
        /* 2/3. Build the URL, Configure the request */
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.getPaymentsForecastForSelectionParams.method))
        
        /* 4. Make the request */
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            
            // if an error occurs, print it and re-enable the UI
            func displayError(_ error: String, debugLabelText: String? = nil) {
                performUIUpdatesOnMain {
                    let alertController = UIAlertController(title: "Bit Client", message:
                        error, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
            func updateTable() {
                performUIUpdatesOnMain {
                    self.tableView.reloadData()
                }
            }
            
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                displayError("Сервер не найден")
                return
            }
            
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                displayError("Ошибка на сервере")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                displayError("Сервер не вернул данные")
                return
            }
            
            /* 5. Parse the data */
            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]
            } catch {
                print("Could not parse the data as JSON: '\(data)'")
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                print("Cannot find key '\(Constants.resultCode)' in \(String(describing: parsedResult))")
                return
            }
            
            if (resultCode != Constants.getPaymentsForecastForSelectionResponse.successCode){
                displayError("Не удалось получить прогноз платежей!")
                return
            }
            
            guard let forecasts = parsedResult[Constants.getPaymentsForecastForSelectionResponse.forecasts] as? [[String:AnyObject]] else {
                return
            }
            
            /* 6. Use the data! */
            self.forecasts = Forecast.forecastsFromResults(forecasts)
            updateTable()
        }
        
        /* 6. Start the request */
        task.resume()
    }
    
    func setForecast(_ forecast: Forecast){
        let methodParameters = [
            Constants.setPaymentForecastParams.id: document.id,
            Constants.setPaymentForecastParams.baseId: document.baseId,
            Constants.setPaymentForecastParams.forecastId: forecast.paymentForecastId,
            Constants.setPaymentForecastParams.forecastAmount: forecast.forecastAmount,
            Constants.setPaymentForecastParams.applicationAmount: forecast.applicationAmount,
            Constants.setPaymentForecastParams.balance: forecast.balance] as [String : AnyObject]
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.setPaymentForecastParams.method))
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
        }
        task.resume()
    }
    
    // -------------------------------------------------------------------------
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfForecasts
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ForecastCell
        if let currentForecast = forecast(at: indexPath) {
            // Configure cell
            cell.forecastLabel.text = currentForecast.paymentForecast
            cell.finCenterLabel.text = currentForecast.finCenter
            cell.forecastAmountLabel.text = "\(currentForecast.forecastAmount)"
            cell.applicationAmountLabel.text = "\(currentForecast.applicationAmount)"
            cell.balanceLabel.text = "\(currentForecast.balance)"
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let setForecast = UITableViewRowAction(style: .normal, title: "Добавить", handler:{(rowAction, indexPath) in
            if let forecast = self.forecast(at: indexPath){
                self.setForecast(forecast)
            }
        })
        setForecast.backgroundColor = UIColor.gray
        
        return [setForecast]
    }
    
    
    // ---------------------------
    // MARK: - Help methods
    
    var numberOfForecasts: Int { return forecasts.count }
    
    func forecast(at indexPath: IndexPath) -> Forecast? {
        if forecasts.count == 0 {
            return nil
        }
        return forecasts[indexPath.row]
    }

}
