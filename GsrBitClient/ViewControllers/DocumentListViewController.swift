//
//  DocumentListViewController.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

class DocumentListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //--------------------------------------
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var setDecisionButton: UIBarButtonItem!
    
    //--------------------------------------
    // MARK: - Properties
    var appDelegate: AppDelegate!
    var documents: [Document] = [Document]()
    var documentType: String = ""
    var selectedDocuments: Set <Document> = []
    
    //--------------------------------------
    // MARK: - Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getDocuments()
        selectedDocuments.removeAll()
        navigationItem.rightBarButtonItem = nil
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        getDocuments()
        selectedDocuments.removeAll()
        navigationItem.rightBarButtonItem = nil
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(DocumentListViewController.longPressGestureRecognized(longPress:)))
        self.tableView.addGestureRecognizer(longPress)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @objc func longPressGestureRecognized(longPress: UILongPressGestureRecognizer) {
        let state = longPress.state
        let location = longPress.location(in: self.tableView)
        guard let indexPath = self.tableView.indexPathForRow(at: location) else {
            return
        }
        switch state {
        case .ended:
            tableView.reloadData()
            break
        case .began:
            guard let cell = self.tableView.cellForRow(at: indexPath) else {
                return
            }
            guard let selectedDocument = document(at: indexPath) else {
                return
            }
            if selectedDocuments.contains(selectedDocument){
                cell.accessoryType = .none
                selectedDocuments.remove(selectedDocument)
            }
            else {
                cell.accessoryType = .checkmark
                selectedDocuments.insert(selectedDocument)
            }
            tableView.deselectRow(at: indexPath, animated: true)
            if selectedDocuments.count != 0 {
                navigationItem.rightBarButtonItem = setDecisionButton
            } else {
                navigationItem.rightBarButtonItem = nil
            }
            break
        default:
            break
        }
    }
    
    //--------------------------------------
    // MARK: - Network requests
    
    private func getDocuments(){
        
        documents.removeAll()
        
        let methodParameters = [
            Constants.getDocumentsParams.token: self.appDelegate.token]
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.getDocumentsParams.method))
        
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in

            func displayError(_ error: String, debugLabelText: String? = nil) {
                print(error)
                performUIUpdatesOnMain {
                    let alertController = UIAlertController(title: "Bit Client", message:
                        error, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
            func updateTable() {
                performUIUpdatesOnMain {
                    self.tableView.reloadData()
                }
            }
            
            guard (error == nil) else {
                displayError("Сервер не найден")
                return
            }

            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                displayError("Ошибка на сервере")
                return
            }
        
            guard let data = data else {
                displayError("Сервер не вернул данные")
                return
            }

            
            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]
            } catch {
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                return
            }
            
            if (resultCode != Constants.getDocumentsResponse.successCode){
                displayError("Не удалось получить список документов!")
                return
            }
            
            guard let documents = parsedResult[Constants.getDocumentsResponse.documents] as? [[String:AnyObject]] else {
                return
            }
            
            self.documents.removeAll()
            let allDocuments = Document.documentsFromResults(documents)
            for document in allDocuments {
                if document.type == self.documentType {
                    self.documents.append(document)
                }
            }
            updateTable()
        }

        task.resume()
    }
    
    // -------------------------------------------------------------------------
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let document = document(at: indexPath){
            performSegue(withIdentifier: "ShowDocument", sender: document)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfDocuments
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DocumentCell.defaultReuseIdentifier, for: indexPath) as! DocumentCell
        if let currentDocument = document(at: indexPath){ 
            // Configure cell
            cell.linkLabel.text = currentDocument.view
            cell.databaseLabel.text = currentDocument.base
            cell.userLabel.text = currentDocument.user
            
            if selectedDocuments.contains(currentDocument){
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    
        let setDecision = UITableViewRowAction(style: .normal, title: "Согласовать", handler:{(rowAction, indexPath) in
            if let document = self.document(at: indexPath){
                self.showDesicionDialog(document)
            }
        })
        setDecision.backgroundColor = UIColor.red
        
        if appDelegate.isTreasurer && document(at: indexPath)?.type == Constants.costDocumentType {
            let setCostDate = UITableViewRowAction(style: .normal, title: "Изменить дату расхода", handler:{(rowAction, indexPath) in
                if let document = self.document(at: indexPath){
                    self.showDatePicker(document)
                }
            })
            setCostDate.backgroundColor = UIColor.gray
            
            let showForecast = UITableViewRowAction(style: .normal, title: "Прогнозы платежей", handler:{(rowAction, indexPath) in
                if let document = self.document(at: indexPath){
                    self.performSegue(withIdentifier: "ShowForecast", sender: document)
                }
            })
            showForecast.backgroundColor = UIColor.darkGray
            
            return [setDecision, setCostDate, showForecast]
        }
        return [setDecision]
    }
    
    //--------------------------------------
    // MARK: - Actions
    
    @IBAction func setGroupDecision(_ sender: Any) {
        showDesicionDialog(nil)
    }
    
    func showDatePicker(_ document: Document){
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        
        let alert = UIAlertController(title: "\n\n\n\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .actionSheet)
        alert.view.addSubview(datePicker)

        let ok = UIAlertAction(title: "Ок", style: .default) { (action) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMdd"
            let dateString = dateFormatter.string(from: datePicker.date)
            self.setCostDate(dateString, document);
            print(dateString)
        }
    
        let cancel = UIAlertAction(title: "Отмена", style: .default, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func setCostDate(_ dateString:String, _ document:Document){
        let methodParameters = [
            Constants.setCostDateParams.id: document.id,
            Constants.setCostDateParams.date: dateString,
            Constants.setCostDateParams.baseId: document.baseId]
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.setCostDateParams.method))
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.getDocuments()
            }
        }
        task.resume()
    }
    
    func showError(){
        let alert = UIAlertController(title: "Установка решения", message: "Комментарий должен быть заполнен!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Хорошо", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func setDecision(document: Document, decision: String, comment: String = ""){
        
        let methodParameters = [
            Constants.setDecisionParams.token: self.appDelegate.token,
            Constants.setDecisionParams.documentId: document.id,
            Constants.setDecisionParams.decision: decision,
            Constants.setDecisionParams.comment: comment]
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.setDecisionParams.method))
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.getDocuments()
            }
        }
        task.resume()
        
    }
    
    func showDesicionDialog(_ document: Document?){
        let alert = UIAlertController(title: "Установка решения", message: "Введите комментарий", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        alert.addAction(UIAlertAction(title: "Уточнить", style: .default, handler: { action in
            let textField = alert.textFields![0] // Force unwrapping because we know it exists.
            let comment = textField.text ?? ""
            if comment.isEmpty {
                self.showError()
                return
            }
            if let document = document{
                self.setDecision(document: document, decision: "Specify", comment: comment)
            } else {
                for document in self.selectedDocuments {
                    self.setDecision(document: document, decision: "Specify", comment: comment)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Отклонить", style: .default, handler: { action in
            let textField = alert.textFields![0] // Force unwrapping because we know it exists.
            let comment = textField.text ?? ""
            if comment.isEmpty {
                self.showError()
                return
            }
            if let document = document{
                self.setDecision(document: document, decision: "Decline", comment: comment)
            } else {
                for document in self.selectedDocuments {
                    self.setDecision(document: document, decision: "Decline", comment: comment)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Согласовать", style: .default, handler: { action in
            let textField = alert.textFields![0] // Force unwrapping because we know it exists.
            let comment = textField.text ?? ""
             if let document = document{
                self.setDecision(document: document, decision: "Approve", comment: comment)
             } else {
                for document in self.selectedDocuments {
                    self.setDecision(document: document, decision: "Approve", comment: comment)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Закрыть", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    


    // -------------------------------------------------------------------------
    // MARK: - Help methods
    
    var numberOfDocuments: Int { return documents.count }
    
    func document(at indexPath: IndexPath) -> Document? {
        if documents.count == 0 {
            return nil
        }
        return documents[indexPath.row]
    }
    
    // -------------------------------------------------------------------------
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? DocumentViewController {
            vc.document = sender as? Document
        }
        
        if let vc = segue.destination as? ForecastListViewController {
            if let document = sender as? Document {
                vc.document = document
            }
        }
    }

}
