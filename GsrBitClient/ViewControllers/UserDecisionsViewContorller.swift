

import UIKit

private let reuseIdentifier = "UserDecisionCell"

class UserDecisionsViewController: UITableViewController {

    // ---------------------------
    // MARK: - Properties
    var appDelegate: AppDelegate!
    var decisions: [UserDecision] = [UserDecision]()

    // ---------------------------
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        getDecisions()
        
    }
    
    // ---------------------------
    // MARK: - Network requests
    
    func getDecisions(){
        /* 1. Set the parameters */
        let methodParameters = [
            Constants.getDecisionsByUserParams.token: appDelegate.token!,
            Constants.getDecisionsByUserParams.showDays:90] as [String : AnyObject]
        
        /* 2/3. Build the URL, Configure the request */
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.getDecisionsByUserParams.method))
        
        /* 4. Make the request */
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            
            // if an error occurs, print it and re-enable the UI
            func displayError(_ error: String, debugLabelText: String? = nil) {
                performUIUpdatesOnMain {
                    let alertController = UIAlertController(title: "Bit Client", message:
                        error, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            }
            
            func updateTable() {
                performUIUpdatesOnMain {
                    self.tableView.reloadData()
                }
            }
            
            
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                displayError("Сервер не найден")
                return
            }
            
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                displayError("Ошибка на сервере")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                displayError("Сервер не вернул данные")
                return
            }
            
            /* 5. Parse the data */
            
            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]
            } catch {
                print("Could not parse the data as JSON: '\(data)'")
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                print("Cannot find key '\(Constants.resultCode)' in \(String(describing: parsedResult))")
                return
            }
            
            if (resultCode != Constants.getDecisionsByUserResponse.successCode){
                displayError("Не удалось получить историю согласования!")
                return
            }
            
            guard let decisions = parsedResult[Constants.getDecisionsByUserResponse.decisions] as? [[String:AnyObject]] else {
                print("Cannot find key '\(Constants.getDecisionResponse.decisions)' in \(parsedResult)")
                return
            }
            
            /* 6. Use the data! */
            self.decisions = UserDecision.decisionsFromResults(decisions)
            
            updateTable()
        }
        
        /* 6. Start the request */
        task.resume()
    }
    
    func getObject(_ decision: UserDecision){
        /* 1. Set the parameters */
        let methodParameters = [
            Constants.getObjectParams.type: decision.type,
            Constants.getObjectParams.baseId: decision.baseId,
            Constants.getObjectParams.documentId: decision.id] as [String : AnyObject]
        
        /* 2/3. Build the URL, Configure the request */
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.getObjectParams.method))
        
        /* 4. Make the request */
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            
            // if an error occurs, print it and re-enable the UI
            func displayError(_ error: String, debugLabelText: String? = nil) {
                performUIUpdatesOnMain {
                    let alertController = UIAlertController(title: "Bit Client", message:
                        error, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            }
            
            func showDocument(_ document: Document) {
                performUIUpdatesOnMain {
                    self.performSegue(withIdentifier: "ShowDocument", sender: document)
                    print(document.view)
                }
            }
            
            
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                displayError("Сервер не найден")
                return
            }
            
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                displayError("Ошибка на сервере")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                displayError("Сервер не вернул данные")
                return
            }
            
            /* 5. Parse the data */
            
            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]
            } catch {
                print("Could not parse the data as JSON: '\(data)'")
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                print("Cannot find key '\(Constants.resultCode)' in \(String(describing: parsedResult))")
                return
            }
            
            if (resultCode != Constants.getObjectResponse.successCode){
                displayError("Не удалось получить историю согласования!")
                return
            }
            
            let document = Document(dictionary: parsedResult[Constants.getObjectResponse.document] as! [String:AnyObject])
            showDocument(document)
            
        }
        
        /* 6. Start the request */
        task.resume()
    }
    
    // -------------------------------------------------------------------------
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfDecisions
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentDecision = decision(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! UserDecisionCell
        
        // Configure cell
        cell.dateLabel.text = currentDecision.date
        cell.databaseLabel.text = currentDecision.base
        cell.linkLabel.text = currentDecision.view
        cell.visaLabel.text = currentDecision.visa + ": " + currentDecision.decision + (currentDecision.comment.isEmpty ? "" : "/" + currentDecision.comment)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        getObject(decision(at: indexPath))
    }
    
    // ---------------------------
    // MARK: - Help methods
    
    var numberOfDecisions: Int { return decisions.count }
    
    func decision(at indexPath: IndexPath) -> UserDecision {
        return decisions[indexPath.row]
    }
    
    
    // -------------------------------------------------------------------------
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DocumentViewController {
            vc.document = sender as! Document
            vc.readOnly = true
        }
    }
}

