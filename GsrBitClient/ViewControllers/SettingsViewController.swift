//
//  SettingsViewController.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController {
    
    @IBOutlet weak var serverAddressTextField: UITextField!
    @IBOutlet weak var serverPortTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restoreVelues()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        saveValues()
    }

    func saveValues(){
        UserDefaults.standard.set(serverAddressTextField.text, forKey: "serverAddress")
        UserDefaults.standard.set(serverPortTextField.text, forKey: "serverPort")
    }
    
    func restoreVelues(){
        serverAddressTextField.text = UserDefaults.standard.string(forKey: "serverAddress")
        serverPortTextField.text = UserDefaults.standard.string(forKey: "serverPort")
    }
}
