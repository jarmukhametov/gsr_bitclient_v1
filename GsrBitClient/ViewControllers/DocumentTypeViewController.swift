
import UIKit

class DocumentTypeViewController: UITableViewController {

    // -------------------------------------------------------------------------
    // MARK: - Variables
    let cellIdentifier = "DocumentTypeCelll"
    var documentTypes :[String: Int] = [:]
    var appDelegate: AppDelegate!
    
    // -------------------------------------------------------------------------
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        getDocuments()
    }
    
    //--------------------------------------
    // MARK: - Network requests
    
    private func getDocuments(){
        
        documentTypes.removeAll()
        
        let methodParameters = [
            Constants.getDocumentsParams.token: self.appDelegate.token]
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.getDocumentsParams.method))

        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            func updateTable() {
                performUIUpdatesOnMain {
                    self.tableView.reloadData()
                }
            }
            guard let data = data else {
                return
            }

            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]

            } catch {
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                return
            }
            
            if (resultCode != Constants.getDocumentsResponse.successCode){
                return
            }
            
            guard let documentsDict = parsedResult[Constants.getDocumentsResponse.documents] as? [[String:AnyObject]] else {
                return
            }
            
            let documents = Document.documentsFromResults(documentsDict)
            for document in documents {
                self.documentTypes[document.type, default: 0] += 1
            }
            updateTable()
        }
        
        /* 6. Start the request */
        task.resume()
    }
    
    // -------------------------------------------------------------------------
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documentTypes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell!
        if let current = tableView.dequeueReusableCell(withIdentifier: cellIdentifier){
            cell = current
        }
        else {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        let currentType = Array(documentTypes)[indexPath.row]
            cell.textLabel!.text = Constants.documentTypes[currentType.key]
            cell.detailTextLabel?.text = "На согласовании: \(currentType.value)"
            cell.imageView?.image = UIImage(named: "document")
            cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowDocumentList", sender: nil)
    }
    
    // -------------------------------------------------------------------------
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DocumentListViewController {
            if let indexPath = tableView.indexPathForSelectedRow {
                vc.documentType = Array(documentTypes)[indexPath.row].key
            }
        }
    }
}
