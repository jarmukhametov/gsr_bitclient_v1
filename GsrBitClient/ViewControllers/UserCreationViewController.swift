//
//  UserCreationViewController.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

class UserCreationViewController: UIViewController{
    
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var appDelegate: AppDelegate!

    
    @IBOutlet weak var passwordConfirmationTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as? AppDelegate

    }
    
    func showError(errorDescription:String){
        let alert = UIAlertController(title: "Регистрация", message: errorDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Хорошо", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
    func showAlert(_ close: Bool, _ message: String){
        let alert = UIAlertController(title: "Создание пользователя", message: message, preferredStyle: .alert)
        if close {
            alert.addAction(UIAlertAction(title: "Хорошо", style: .default, handler: {
                action in
                self.navigationController?.popViewController(animated: true)
            }))

        }
        else {
            alert.addAction(UIAlertAction(title: "Хорошо", style: .default, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func createUser(login: String, password: String){
        let md5Data = MD5(string:password)
        let md5Base64 = md5Data.base64EncodedString()
        let md5Hex =  md5Data.map { String(format: "%02hhx", $0) }.joined()
        let methodParameters = [ Constants.createUserParams.login: login,
                                 Constants.createUserParams.passwordHash: md5Hex]
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.createUserParams.method))
        
        print("Url: \(request)")

        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            
            func showAlert(_ close:Bool, _ message: String){
                performUIUpdatesOnMain {
                        self.showAlert(close, message)
                }
            }
            guard let data = data else {
                return
            }
            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]
                print("Result data: '\(String(describing: parsedResult))'")
            } catch {
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                print("Cannot find key '\(Constants.resultCode)' in \(String(describing: parsedResult))")
                return
            }
            
            if (resultCode == Constants.createUserParams.successCode){
                //  Open documents list
                showAlert(true, "Пользователь успешно создан")
            }
            if (resultCode == Constants.createUserParams.doubleErrorCode){
                //  Open documents list
                showAlert(false, "Пользователь уже существует")
            }
            
            if (resultCode == Constants.createUserParams.errorCode){
                //  Open documents list
                showAlert(false, "Не удалось создать пользователя")
            }
            
        }
        task.resume()
    }
    
    @IBAction func register(_ sender: Any) {
        
        guard let login = userTextField.text else {
            showError(errorDescription: "Не заполнено имя пользователя")
            return
        }
        
        if login.count < 4 {
            showError(errorDescription: "Логин должен содержать не менее 4 символов")
            return
        }
        
        guard let password = passwordTextField.text else {
            showError(errorDescription: "Не заполнен пароль!")
            return
        }
        
        guard let passwordConfirmation = passwordConfirmationTextField.text else {
            showError(errorDescription: "Не заполнено подтверждение пароля!")
            return
        }
        
        if password != passwordConfirmation {
            showError(errorDescription: "Введенные пароли не совпадают!")
            return
        }
        
        if password.count < 4 {
            showError(errorDescription: "Пароль должен содержать не менее 4 символов!")
            return
        }
        
        createUser(login: login, password: password)
    }
    
}
