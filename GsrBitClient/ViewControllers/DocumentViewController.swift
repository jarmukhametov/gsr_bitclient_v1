//
//  DocumentViewController
//  GSR_Bit_Client
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit
class DocumentViewController: UIViewController, UITableViewDataSource {

    var document: Document!
    var readOnly = false
    var appDelegate: AppDelegate!
    var fields: [Field]?
    
    @IBOutlet weak var tableView: UITableView!


    @IBOutlet weak var labelDocument: UILabel!
    @IBOutlet weak var labelDatabase: UILabel!
    @IBOutlet weak var labelUser: UILabel!
    @IBOutlet weak var setDecision: UINavigationItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate

        if readOnly {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    func showError(){
        let alert = UIAlertController(title: "Установка решения", message: "Комментарий должен быть заполнен!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Хорошо", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    
    func setDecision(decision: String, comment: String = ""){
        
        let methodParameters = [
            Constants.setDecisionParams.token: self.appDelegate.token,
            Constants.setDecisionParams.documentId: document.id,
            Constants.setDecisionParams.decision: decision,
            Constants.setDecisionParams.comment: comment]
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.setDecisionParams.method))
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)            }
        }
        task.resume()

    }
    
    @IBAction func showDesicionDialog(){
        let alert = UIAlertController(title: "Установка решения", message: "Введите комментарий", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        alert.addAction(UIAlertAction(title: "Уточнить", style: .default, handler: { action in
            let textField = alert.textFields![0] // Force unwrapping because we know it exists.
            let comment = textField.text ?? ""
            if comment.isEmpty {
                self.showError()
                return
            }
            self.setDecision(decision: "Specify", comment: comment)
        }))
        alert.addAction(UIAlertAction(title: "Отклонить", style: .default, handler: { action in
            let textField = alert.textFields![0] // Force unwrapping because we know it exists.
            let comment = textField.text ?? ""
            if comment.isEmpty {
                self.showError()
                return
            }
            self.setDecision(decision: "Decline", comment: comment)
        }))
        alert.addAction(UIAlertAction(title: "Согласовать", style: .default, handler: { action in
            let textField = alert.textFields![0] // Force unwrapping because we know it exists.
            let comment = textField.text ?? ""
            self.setDecision(decision: "Approve", comment: comment)
        }))
        alert.addAction(UIAlertAction(title: "Закрыть", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
	
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    
    func updateUI() {
        labelDocument.text = document.view
        labelUser.text = document.user
        labelDatabase.text = document.base
        
        let data = document.data.data(using: .utf8)!
        let parsedResult: [String:AnyObject]!
        do {
            parsedResult = try JSONSerialization.jsonObject(with: data , options: .allowFragments) as! [String:AnyObject]
            
        } catch {
            print("Could not parse the data as JSON: '\(document.data)'")
            return
        }
        guard let fieldsArray = parsedResult["fields"] as? [[String:AnyObject]] else {
            print("Cannot find key 'fields' in \(parsedResult)")
            return
        }
        
        fields = Field.fieldsFromResults(fieldsArray)
        
    }
    
    // -------------------------------------------------------------------------
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfFields
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FieldCell", for: indexPath) as! FieldCell

        guard let currentField = field(at: indexPath) else {
            return cell
        }
        
        // Configure cell
        cell.fieldLabel.text = currentField.description + ": " + currentField.value
        
        return cell
    }
    
    // Helper
    
    var numberOfFields: Int {
        if fields != nil {
            return fields!.count
        }
        else {
            return 0
        }
    }
    
    func field(at indexPath: IndexPath) -> Field? {
        if fields != nil {
            return fields![indexPath.row]
        }
        else {
            return nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? FilesViewController {
            vc.document = document
        }
        else if let vc = segue.destination as? DecisionViewController{
            vc.document = document
        }
    }
    
    
}
