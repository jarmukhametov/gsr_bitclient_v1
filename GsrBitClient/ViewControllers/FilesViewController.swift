//
//  FilesViewController.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

private let reuseIdentifier = "FileCell"

class FilesViewController: UITableViewController {
    
    // MARK: Properties
    var appDelegate: AppDelegate!
    var document: Document!
    var files: [File] = [File]()
    
    let documentInteractionController = UIDocumentInteractionController()

    override func viewDidLoad() {
        super.viewDidLoad()

        appDelegate = UIApplication.shared.delegate as? AppDelegate
        getFiles()

        documentInteractionController.delegate = self

    }
    
    private func getFiles(){
    
        /* 1. Set the parameters */
        
        let methodParameters = [
            Constants.getFilesParams.documentId: document.id]
        
        /* 2/3. Build the URL, Configure the request */
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.getFilesParams.method))
        //print("Url: \(request)")
        
        /* 4. Make the request */
        let task = appDelegate.sharedSession.dataTask(with: request) { (data, response, error) in
            
            // if an error occurs, print it and re-enable the UI
            func displayError(_ error: String, debugLabelText: String? = nil) {
                print(error)
                performUIUpdatesOnMain {
                    let alertController = UIAlertController(title: "Bit Client", message:
                        error, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            }
            
            func updateTable() {
                performUIUpdatesOnMain {
                    self.tableView.reloadData()
                }
            }
            
            
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                displayError("Сервер не найден")
                return
            }
            
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                displayError("Ошибка на сервере")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                displayError("Сервер не вернул данные")
                return
            }
            
            /* 5. Parse the data */
            
            let parsedResult: [String:AnyObject]!
            do {
                parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject]
                //print("Result data: '\(String(describing: parsedResult))'")
            } catch {
                print("Could not parse the data as JSON: '\(data)'")
                return
            }
            
            guard let resultCode = parsedResult![Constants.resultCode] as? String else {
                print("Cannot find key '\(Constants.resultCode)' in \(String(describing: parsedResult))")
                return
            }
            
            if (resultCode != Constants.getFilesResponse.successCode){
                displayError("Не удалось получить список файлов!")
                return
            }
            
            guard let files = parsedResult[Constants.getFilesResponse.files] as? [[String:AnyObject]] else {
                print("Cannot find key '\(Constants.getDocumentsResponse.documents)' in \(parsedResult)")
                return
            }
            
            /* 6. Use the data! */
            self.files = File.filesFromResults(files)
            
            updateTable()
        }
        
        /* 6. Start the request */
        task.resume()
    }
    
    func startDownload(url: URL, _ file: File) {
        let download = Download(url: url)
        let downloadsSession = URLSession(configuration: .default)
        download.task = downloadsSession.downloadTask(with: url){
            (location, response, error) in
            if let error = error{
                print("There is error due load: \(error)")
            }
            self.saveDownload(download: download, location: location, response: response, error: error, file)
        }
        download.task?.resume()
        download.isDownloading = true
        //activeDownloads[download.url] = download
    }
    
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL, _ file: File) -> URL {
        return documentsPath.appendingPathComponent(file.fileName)
    }
    
    func saveDownload(download : Download, location : URL?, response : URLResponse?, error : Error?, _ file: File) {
        let sourceURL = download.url
        if error != nil { return }
        
        //activeDownloads[sourceURL] = nil
        
        let destinationURL = localFilePath(for: sourceURL, file)
        
        let fileManager = FileManager.default
        try? fileManager.removeItem(at: destinationURL)
        do {
            try fileManager.copyItem(at: location!, to: destinationURL)
            print("File saved to \(destinationURL)")
        } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
        
        DispatchQueue.main.async {
            /// STOP YOUR ACTIVITY INDICATOR HERE
            self.share(url: destinationURL)
        }
    }
    
    func getFile(file : File){
    
        /* 1. Set the parameters */
        let methodParameters = [
            Constants.getFileParams.fileId: file.id,
            Constants.getFileParams.documentId: file.documentId,
            Constants.getFileParams.baseId: file.baseId]
        
        /* 2/3. Build the URL, Configure the request */
        let request = URLRequest(url: appDelegate.URLFromParameters(methodParameters as [String:AnyObject], withPathExtension: Constants.getFileParams.method))
        //print("Url: \(request)")
        startDownload(url: request.url!, file)

    }



 
    // -------------------------------------------------------------------------
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfFiles
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentFile = file(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileCell", for: indexPath) as! FileCell
        
        // Configure cell
        cell.titleLabel.text = currentFile.fileName
        cell.descriptionLabel.text = currentFile.description
        cell.delegate = self
        
        return cell
    }
    
    // Helper
    
    var numberOfFiles: Int { return files.count }
    
    func file(at indexPath: IndexPath) -> File {
        return files[indexPath.row]
    }

}

extension FilesViewController: FileCellDelegate{
    func actionPressed(_ fileCell: FileCell) {
        if let indexPath = tableView.indexPath(for: fileCell){
            let file = files[indexPath.row]
            getFile(file: file)
        }
    }
}

extension FilesViewController {
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentPreview(animated: true)
    }
}

extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}

extension FilesViewController: UIDocumentInteractionControllerDelegate {
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}
