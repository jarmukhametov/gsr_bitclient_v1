//
//  Field.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import Foundation

struct Field {
    let description: String
    let value: String
    
    init(dictionary: [String:AnyObject]) {
        description = dictionary[Constants.documentFields.description] as! String
        value = dictionary[Constants.documentFields.value] as! String
    }
    
    static func fieldsFromResults(_ results: [[String:AnyObject]]) -> [Field] {
        
        var fields = [Field]()
        
        for result in results {
            fields.append(Field(dictionary: result))
        }
        
        return fields
    }
}
