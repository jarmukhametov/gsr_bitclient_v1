import Foundation

struct UserDecision {
    let id: String
    let base: String
    let baseId: String
    let number: String
    let view: String
    let date: String
    let type: String
    let visa: String
    let comment: String
    let decision: String
    
    init(dictionary: [String:AnyObject]) {
        id = dictionary[Constants.userDecision.id] as! String
        base = dictionary[Constants.userDecision.base] as! String
        baseId = dictionary[Constants.userDecision.baseId] as! String
        number = dictionary[Constants.userDecision.number] as! String
        view = dictionary[Constants.userDecision.view] as! String
        date = dictionary[Constants.userDecision.date] as! String
        type = dictionary[Constants.userDecision.type] as! String
        visa = dictionary[Constants.userDecision.type] as! String
        comment = dictionary[Constants.userDecision.comment] as! String
        decision = dictionary[Constants.userDecision.decision] as! String
    }
    
    static func decisionsFromResults(_ results: [[String:AnyObject]]) -> [UserDecision] {
        
        var decisions = [UserDecision]()
        
        for result in results {
            decisions.append(UserDecision(dictionary: result))
        }
        
        return decisions
    }
}
