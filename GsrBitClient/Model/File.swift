//
//  File.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import Foundation

struct File {
    let id: String
    let documentId: String
    let baseId: String
    let fileName: String
    let description: String
    var url: String
    var isDownloaded: Bool
    var isDownloading: Bool
    
    init(dictionary: [String:AnyObject]) {
        id = dictionary[Constants.file.id] as! String
        documentId = dictionary[Constants.file.documentId] as! String
        baseId = dictionary[Constants.file.baseId] as! String
        fileName = dictionary[Constants.file.fileName] as! String
        description = dictionary[Constants.file.description] as! String
        url = ""
        isDownloading = false
        isDownloaded = false
    }
    
    static func filesFromResults(_ results: [[String:AnyObject]]) -> [File] {
        
        var files = [File]()
        
        for result in results {
            files.append(File(dictionary: result))
        }
        
        return files
    }
}
