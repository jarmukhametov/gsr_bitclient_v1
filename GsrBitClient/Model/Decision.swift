//
//  File.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import Foundation

struct Decision {
    let date: String
    let user: String
    let visa: String
    let comment: String
    let decision: String
    
    init(dictionary: [String:AnyObject]) {
        date = dictionary[Constants.decision.date] as! String
        user = dictionary[Constants.decision.user] as! String
        visa = dictionary[Constants.decision.visa] as! String
        comment = dictionary[Constants.decision.comment] as! String
        decision = dictionary[Constants.decision.decision] as! String
    }
    
    static func decisionsFromResults(_ results: [[String:AnyObject]]) -> [Decision] {
        
        var decisions = [Decision]()
        
        for result in results {
            decisions.append(Decision(dictionary: result))
        }
        
        return decisions
    }
}
