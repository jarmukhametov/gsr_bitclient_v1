//
//  Document.swift
//  GSR_Bit_Client
//
//  Copyright © 2018 GSR. All rights reserved.
//

import Foundation

struct Document {
    
    // MARK: Properties

    let id: String
    let user: String
    let base: String
    let number: String
    let date: String
    let view: String
    let data: String
    let userId: String
    let baseId: String
    let type: String
   
        
    // MARK: Initializers
    
    init(dictionary: [String:AnyObject]) {
        id = dictionary[Constants.document.id] as! String
        user = dictionary[Constants.document.user] as! String
        base = dictionary[Constants.document.base] as! String
        number = dictionary[Constants.document.number] as! String
        date = dictionary[Constants.document.date] as! String
        view = dictionary[Constants.document.view] as! String
        data = dictionary[Constants.document.data] as! String
        userId = dictionary[Constants.document.userId] as! String
        baseId = dictionary[Constants.document.baseId] as! String
        type = dictionary[Constants.document.type] as! String
        //fields = dictionary[Constants.document.fields] as! [Field]
    }
    
    static func documentsFromResults(_ results: [[String:AnyObject]]) -> [Document] {
        var documents = [Document]()
        for result in results {
            documents.append(Document(dictionary: result))
        }
        return documents
    }
}

extension Document: Equatable {
    static func == (lhs: Document, rhs: Document) -> Bool {
        return lhs.id == rhs.id
    }
}

extension Document: Hashable {
    var hashValue: Int {
        return id.hashValue
    }
}
