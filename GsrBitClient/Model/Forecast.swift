//
//  Forecast.swift
//  GsrBitClient
//
//  Created by Admin on 27.06.2019.
//  Copyright © 2019 GSR. All rights reserved.
//

import Foundation


struct Forecast {
    let paymentForecast: String
    let paymentForecastId: String
    let forecastAmount: Double
    let applicationAmount: Double
    let balance: Double
    let finCenter: String
    
    init(dictionary: [String:AnyObject]) {
        paymentForecast = dictionary[Constants.forecast.paymentForecast] as! String
        paymentForecastId = dictionary[Constants.forecast.paymentForecastId] as! String
        forecastAmount = dictionary[Constants.forecast.forecastAmount] as! Double
        applicationAmount = dictionary[Constants.forecast.applicationAmount] as! Double
        balance = dictionary[Constants.forecast.balance] as! Double
        finCenter = dictionary[Constants.forecast.finCenter] as! String
    }
    
    static func forecastsFromResults(_ results: [[String:AnyObject]]) -> [Forecast] {
        
        var forecasts = [Forecast]()
        
        for result in results {
            forecasts.append(Forecast(dictionary: result))
        }
        
        return forecasts
    }
}
