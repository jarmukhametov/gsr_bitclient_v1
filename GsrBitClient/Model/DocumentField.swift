//
//  DocumentField.swift
//  GSR_Bit_Client
//
//  Created by User on 14/10/2018.
//  Copyright © 2018 GSR. All rights reserved.
//

import Foundation

struct DocumentFields {
    
    // MARK: Properties
    
    let description: String
    let value: String
    
    // MARK: Initializers
    
    init(dictionary: [String: AnyObject]){
        description = dictionary[Constants.documentFields.description] as! String
        value = dictionary[Constants.documentFields.value] as! String
    }
    
    static func documentFieldsFromResults(_ results: [[String:AnyObject]]) -> [DocumentFields] {
        
        var documentFields = [DocumentFields]()
        
        for result in results {
            documentFields.append(DocumentFields(dictionary: result))
        }
        
        return documentFields
    }
}
