//
//  AppDelegate.swift
//  GSR_Bit_Client
//
//  Created by User on 19.09.2018.
//  Copyright © 2018 GSR. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var sharedSession = URLSession.shared
    var token: String? = nil
    var userID: String? = nil
    var isTreasurer: Bool = false
    
}

// MARK: Create URL from Parameters

extension AppDelegate {
    
    func URLFromParameters(_ parameters: [String:AnyObject]?, withPathExtension: String? = nil) -> URL {
    
        var components = URLComponents()
        components.scheme = Constants.gsrServer.ApiScheme
        components.host = Constants.gsrServer.ApiHost
        components.path = Constants.gsrServer.ApiPath + (withPathExtension ?? "")
        components.user = Constants.gsrServer.ApiUser
        components.password = Constants.gsrServer.ApiPassword
        components.port = Constants.gsrServer.ApiPort
        components.queryItems = [URLQueryItem]()
        
        if let parameters = parameters {
            for (key, value) in parameters {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                components.queryItems!.append(queryItem)
            }
        }
        return components.url!
    }
}

